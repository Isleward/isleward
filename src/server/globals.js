global.io = require('./db/io');
global.extend = require('./misc/clone');
global.cons = require('./security/connections');
global._ = require('./misc/helpers');
global.atlas = require('./world/atlas');
global.clientConfig = require('./config/clientConfig');

global.consts = require('./config/consts');
global.balance = require('./config/balance').balance;
