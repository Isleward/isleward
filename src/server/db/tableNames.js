const tableNames = [
	'accountInfo',
	'character',
	'characterList',
	'customChannels',
	'error',
	'login',
	'modLog',
	'recipes',
	'stash',
	'events'
];

module.exports = tableNames;
